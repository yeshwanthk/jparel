package com.rel.JPARel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaRelApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaRelApplication.class, args);
	}

}
