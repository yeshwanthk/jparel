package com.rel.JPARel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="course")
public class Course {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name = "course_name")
	private String cName;
	
	@ManyToOne(optional=true)
	@JoinColumn(name= "Lerner_Id")
	private Lerner lerner;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public Lerner getLerner() {
		return lerner;
	}

	public void setLerner(Lerner lerner) {
		this.lerner = lerner;
	}
	
	

}
