package com.rel.JPARel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rel.JPARel.model.Course;
import com.rel.JPARel.model.Lerner;
import com.rel.JPARel.repo.CourseRepo;
import com.rel.JPARel.repo.LernerRepo;

@RestController
@RequestMapping(value = "/course")
public class CourseController {

	@Autowired
	private CourseRepo courseRepo;
	
	@Autowired
	private LernerRepo lernerRepo;

	@PostMapping(value ="/create")
	public ResponseEntity<String> addCourse(@RequestBody Course course) {
		Lerner l=lernerRepo.findById(1);
		course.setLerner(l);
		courseRepo.save(course);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@PostMapping(value ="/createLearner")
	public ResponseEntity<String> addLearner(@RequestBody Lerner lerner) {
		lernerRepo.save(lerner);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@GetMapping(value="/getLerner/{name}")
	public ResponseEntity<Lerner> findLernerByName(@PathVariable(name="name") String name){
		System.out.println("Name ::::"+name);
		Lerner l=lernerRepo.findBylName(name);
		return new ResponseEntity<Lerner>(l,HttpStatus.OK);
	}
	@GetMapping(value="/getCourse/{cName}")
	public ResponseEntity<Course> getCourseByName(@PathVariable("cName") String cName){
		Course course=courseRepo.findBycName(cName);
		return new ResponseEntity<Course>(course,HttpStatus.OK);
	}
}
