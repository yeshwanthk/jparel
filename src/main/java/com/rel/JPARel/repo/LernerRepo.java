package com.rel.JPARel.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rel.JPARel.model.Lerner;

public interface LernerRepo extends JpaRepository<Lerner,Integer> {
	
	Lerner findById(int id);
	
	Lerner findBylName(String name);

}
