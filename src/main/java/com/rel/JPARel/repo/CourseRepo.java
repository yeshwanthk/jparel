package com.rel.JPARel.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rel.JPARel.model.Course;

public interface CourseRepo extends JpaRepository<Course, Integer> {
 
	Course findBycName(String cName);
}
